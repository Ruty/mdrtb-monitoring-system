import Vue from 'vue'
import Router from 'vue-router'
import DataShow from '@/components/DataShow'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
        path: '/',
        redirect: '/login',
    },
    {
        name: 'Login',
        path: '/login',
        component: Login,
    },
    {
      path: '/DataShow',
      name: 'DataShow',
      component: DataShow
    }
  ]
})
